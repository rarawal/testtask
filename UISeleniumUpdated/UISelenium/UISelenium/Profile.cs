﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UISelenium.Extension;

namespace UISelenium
{
    class Profile
     {
        private IWebDriver driver;        
        private const string logOutXid = "signout";        
        private OpenQA.Selenium.IWebElement logOutElement;

        public Profile(IWebDriver driver)
        {            
            this.driver = driver;
            
        }

        public void LogOut()
        {
            logOutElement = driver.WaitForElementById("logout-link");                        
            logOutElement.Click();            
        }
    }

}
