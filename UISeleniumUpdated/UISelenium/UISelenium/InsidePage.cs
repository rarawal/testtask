﻿using OpenQA.Selenium;
using System;
using UISelenium.Extension;

namespace UISelenium
{
    class InsidePage
    {
        private IWebDriver driver;
        private string title;

        public InsidePage(IWebDriver driver)
        {            
            this.driver = driver;
        }

        public string PageTitle
        {
            get
            {
                return title;
            }
            set
            {
                title = value;
            }
        }

        public Page TypePageTitle()
        {
            var titleId = this.driver.WaitForElementById("content-title");
            titleId.SendKeys(this.PageTitle);
            var page = new Page(this.driver);
            page.PageTitle = this.PageTitle;
            return page;
        }

        public bool ClickOnCreateNewPage()
        {
            var profileButton = driver.WaitForElementById("quick-create-page-button");
            profileButton.Click();

            for (int i = 0; i <20; i++)
            {
                if (driver.Url.Contains("createpage.action?spaceKey="))
                {
                    return true;
                }
                else
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                    continue;
                }
            }
            return false;
        }

    }
}
