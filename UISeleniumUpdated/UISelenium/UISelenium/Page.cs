﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using UISelenium.Extension;

namespace UISelenium
{
    class Page
    {
        private IWebDriver driver;
        private string _title;

        //BAD BAD BAD design
        public string PageTitle
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public Page(IWebDriver driver)
        {            
            this.driver = driver;
        }

        public bool RemoveRestrictions()
        {
            OpenRestrictionsMenu();

            var selectRestriction = this.driver.WaitForElementByClassName("aui-iconfont-unlocked");
            Assert.IsNotNull(selectRestriction);
            selectRestriction.Click();

            var clickApply = this.driver.WaitForElementById("page-restrictions-dialog-save-button");
            Assert.IsNotNull(clickApply);
            clickApply.Click();

            var permissionsExit = this.driver.WaitForElementById("content-metadata-page-restrictions");
            var restrictionName = permissionsExit.GetAttribute("original-title");
            if (restrictionName.ToLower() == "Unrestricted")
            {
                return true;
            }

            return false;
        }

        public bool SetRestrictions()
        {
            // This is bad method. It is doing of actions. It is not verifying anything.
            OpenRestrictionsMenu();

            //duplicate code
            var selectRestriction = this.driver.WaitForElementByClassName("aui-iconfont-locked");
            Assert.IsNotNull(selectRestriction);
            selectRestriction.Click();

            var clickApply = this.driver.WaitForElementById("page-restrictions-dialog-save-button");
            Assert.IsNotNull(clickApply);
            clickApply.Click();

            var restrictionName = driver.FindElement(By.XPath(".//*[contains(@class, 'aui-icon') and contains(@class, 'aui-icon-small') and contains(@class, 'aui-iconfont-locked')]")); // flexible, match "bighead small crb", "bighead crb", "crb bighead", etc.

            if (!String.IsNullOrEmpty(restrictionName.ToString()))
            {
                return true;
            }

            return false;
        }

        public Page ClickSave()
        {
            var saveButton = this.driver.WaitForElementById("rte-button-publish");
            saveButton.Click();
            for (int i = 0; i < 20; i++)
            {
                if (driver.Url.Contains(this.PageTitle))
                {
                    return this;
                }
                else
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(1));
                    continue;
                }
            }

            return null;
        }
     
        private void OpenRestrictionsMenu()
        {
            var clickMore = this.driver.WaitForElementById("action-menu-link");
            Assert.IsNotNull(clickMore);
            clickMore.Click();

            var clickRestrictionsMenu = this.driver.WaitForElementById("action-page-permissions-link");
            Assert.IsNotNull(clickRestrictionsMenu);
            clickRestrictionsMenu.Click();

            var clickRestrictionsDropDown = this.driver.WaitForElementById("s2id_page-restrictions-dialog-selector");
            Assert.IsNotNull(clickRestrictionsDropDown);
            clickRestrictionsDropDown.Click();
        }
    }
}
