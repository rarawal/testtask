﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Support.Events;
using System.Configuration;


namespace UISelenium
{
    internal static class Driver 
    {
        static Driver()
        {
            InternetExplorerInstance  = new EventFiringWebDriver(GetInternetExplorerWebDriver()); ;
            FireFoxInstance = new EventFiringWebDriver(GetFirefoxWebDriver()); ;
        }

        public static void Quit()
        {
            InternetExplorerInstance.Quit();
            FireFoxInstance.Quit();
        }

        private static IWebDriver GetFirefoxWebDriver()
        {
            var firefoxProfile = new FirefoxProfile
            {
                AcceptUntrustedCertificates = true,
                EnableNativeEvents = true,
                AlwaysLoadNoFocusLibrary = true,
            };          
            var firefoxDriver = new FirefoxDriver(firefoxProfile);
            var options = firefoxDriver.Manage();
            options.Cookies.DeleteAllCookies();
            options.Window.Maximize();
            return firefoxDriver;
        }

        private static IWebDriver GetInternetExplorerWebDriver()
        {
            string pathContainingIEDriverServer = ConfigurationManager.AppSettings["IEDriverPath"]; 

            var internetExplorerProfile = new InternetExplorerOptions()
            {
               
                InitialBrowserUrl = "www.bing.com",
                IntroduceInstabilityByIgnoringProtectedModeSettings = true
            };

            
            InternetExplorerDriver internetExplorerDriver = new InternetExplorerDriver(pathContainingIEDriverServer, internetExplorerProfile);
            var options = internetExplorerDriver.Manage();
            options.Cookies.DeleteAllCookies();
            options.Window.Maximize();
            return internetExplorerDriver;
        }
        public static IWebDriver InternetExplorerInstance { get; private set; }
        public static IWebDriver FireFoxInstance { get; private set; }
    }
}
