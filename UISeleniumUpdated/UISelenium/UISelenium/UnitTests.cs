﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;

namespace UISelenium
{
    [TestClass]    
    public class UnitTests
    {
        static LandingPage landingPage = null;
        static InsidePage insidePage = null;
        // This test is fragile as i havent taken into account what if it takes time to load the page
        // and execution of step isnt finished in that time, i would actually attempt retires here but for now
        // i am keeping it just like this. 

        // There is no logging

        // There are no actual asserts

        // Not fit for localization

        [ClassInitialize]
        public static void ClassInit(TestContext context)
        {
            // Methods mentioned here publically exposed which is bad design,  would rather create an interface and expose only what is required
            Driver.FireFoxInstance.Manage().Cookies.DeleteAllCookies();
          //  Driver.InternetExplorerInstance.Manage().Cookies.DeleteAllCookies();

            // I need to create a enum that will declare all browsers and then in loop will call all of browers randomyl for each test case - Pending
            // I would find an element on landing page rather than wait but this is temporary purpose only
            //System.Threading.Thread.Sleep(TimeSpan.FromSeconds(10));
            landingPage = new LandingPage();
            insidePage = landingPage.OpenLoginPage(LoginType.userName)
                                    .SignIn(ConfigurationManager.AppSettings["username"].ToString(), 
                                            ConfigurationManager.AppSettings["password"].ToString());
        }

        [TestInitialize]
        public void TestInit()
        {
            
        }
        
        // Warning need to be removed - style cop
        [TestMethod]
        public void CreateNewPage()
        {
            if (CreateNewPageInternal() == null)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void SetRestrictionOnPage()
        {
            Page newPage = null;
            bool IsRestrictionsSet = false;

            newPage = CreateNewPageInternal();

            if (newPage != null)
            {
              IsRestrictionsSet = newPage.SetRestrictions();
            }

            if(!IsRestrictionsSet)
            {
                Assert.Fail();
            }

            // This is just for safety purpose to make sure test run after this test has enough time
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(5));            
        }

        private Page CreateNewPageInternal()
        {
            Page newPage = null;

            if (insidePage != null && insidePage.ClickOnCreateNewPage())
            {
                insidePage.PageTitle = Guid.NewGuid().ToString();
                newPage = insidePage.TypePageTitle().ClickSave();
            }

            return newPage;
        }

        [TestCleanup]
        public void TestCleanup()
        {
            landingPage.Profile().LogOut();
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            Driver.Quit();
        }

        [AssemblyCleanup]
        public static void ClosingDriver()
        {
            Driver.FireFoxInstance.Dispose();
            Driver.InternetExplorerInstance.Dispose();
        }

    }
}



