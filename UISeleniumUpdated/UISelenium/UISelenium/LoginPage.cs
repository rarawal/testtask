﻿using System;
using OpenQA.Selenium;
using UISelenium.Extension;

namespace UISelenium
{
    internal class LoginPage
    {
        private readonly IWebDriver driver;
        private readonly Enum loginParameter;
        private IWebElement loginButton;
        public IWebElement userNameElement;
        public IWebElement passwordElement;

        private const string userNameId = "username";
        private const string passwordId = "password";
        private const string loginId = "login";
        
        public LoginPage(IWebDriver driver, Enum loginParameter)
        {
            this.driver = driver;
            this.loginParameter = loginParameter;
        }

        public InsidePage SignIn(string userName, string password)
        {

            if (loginParameter.Equals(LoginType.userName))
            {
                userNameElement = driver.WaitForElementById(userNameId);
                userNameElement.SendKeys(userName);
                passwordElement = driver.WaitForElementById(passwordId);
                passwordElement.SendKeys(password);
                loginButton = driver.WaitForElementById(loginId);               
            }
            loginButton.Click();
            return new InsidePage(driver);            
        }

        public IWebElement ExistPasswordFieldOnPage()
        {
            return driver.WaitForElementVisibleByAttributeExists("id", passwordId);            
        }

        public InsidePage InsertPasswordForMicrosoft(string password)
        {
            passwordElement = driver.WaitForElementById(passwordId);
            passwordElement.SendKeys(password);
            loginButton = driver.WaitForElementById(loginId);
            loginButton.Click();
            return new InsidePage(driver);
        }

        public InsidePage WithoutCredentials()
        {
            return new InsidePage(driver);
        }
      

    }
}