﻿using System;
using OpenQA.Selenium;
using UISelenium.Extension;
using System.Configuration;

namespace UISelenium
{    
    internal class LandingPage
    {
        public IWebElement loginButton;
        private readonly IWebDriver driver;

        // This is Horrible. It has to be put in config file rather than here.
        public LandingPage()
        {
            this.driver = Driver.FireFoxInstance;
            // I should move this configuration manager as seperate class and just create an instance of that class rather spilling it in each file
            driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["landingPageUrl"]);
        }

        public LoginPage OpenLoginPage(Enum loginParameter)
        {
            try
            {
                if (loginParameter.Equals(LoginType.userName))
                {
                    loginButton = driver.WaitForElementById("login");
                }
                return new LoginPage(driver, loginParameter);

            }
            catch
            {
                driver.Quit();
                // This pretty much makes no sense. we should may be declare variable in LoginPage Instance as nullable. or handle it some other way
                return new LoginPage(driver, loginParameter);
            }
        }

        public Profile Profile()
        {
            var profileButton = driver.WaitForElementById("user-menu-link");
            profileButton.Click();
            return new Profile(driver);
        }

    }
}
