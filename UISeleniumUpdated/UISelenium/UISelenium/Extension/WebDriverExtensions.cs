﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace UISelenium.Extension
{
    public static class WebDriverExtensions
    {

        public static IWebElement WaitForElementById(this IWebDriver driver, string id, double waitInSeconds = 20)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitInSeconds));
            return wait.Until(ExpectedConditions.ElementIsVisible(By.Id(id)));
        }

        public static IWebElement WaitForElementByClassName(this IWebDriver driver, string className, double waitInSeconds = 10)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(waitInSeconds));
            return wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName(className)));
        }

        public static IWebElement WaitForElementVisibleByAttributeExists(this IWebDriver driver, string attributeType, string attributeString, double searchCycle = 10)
        {
            IWebElement emptyElement = null;
            int a = 0;
            double timeout = 10; // if I did not wait on start 10 seconds, then it did not find element
            while (a < searchCycle)
            {
                try
                {
                    System.Threading.Thread.Sleep(TimeSpan.FromSeconds(timeout));
                    a++;
                    timeout = timeout / 2;
                    return driver.FindElement(By.CssSelector(String.Format("[{0}='{1}']", attributeType, attributeString)));
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0} Exception caught.", e);
                }
            }
            return emptyElement;
        }
    }

}
